package stevengantz.shared;

/**
 * @author Steven Gantz
 * @date 4/16/2016
 */
public class CurrentGame {
    
    public int totalPlayers = 0;
    
    // How many clicked start
    public int clickedStart = 1;
    
    public CurrentGame(int totalPlayers){
        this.totalPlayers = totalPlayers;
    }
    
}
